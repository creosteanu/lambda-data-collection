import * as _ from 'lodash';

import {getLastData, getRoomLinearDistance} from './utils';

function removeDuplicates(portals) {
	return _.transform(portals, (acc, from, to) => {
		if (!acc[to]) {
			acc[from] = to;
		}
	}, {});
}

function inReachOfRooms({rooms, portal, distance}) {
	const {from, to} = portal;

	return _.some(rooms, (data, roomName) => getRoomLinearDistance(roomName, from) <= distance || getRoomLinearDistance(roomName, to) <= distance);
}

async function filterForPlayer({awsCredentials, portals, player, distance, shard}) {
	if (!player) {
		return portals;
	}

	let rooms = await getLastData(awsCredentials, 'rooms');
	rooms = rooms[shard];
	let playerRooms = _.pickBy(rooms, room => room.owner.toLowerCase() === player.toLowerCase());

	return _.pickBy(portals, (from, to) => inReachOfRooms({rooms: playerRooms, portal: {from, to}, distance}));
}

async function filterForAlliance({awsCredentials, portals, alliance, distance, shard}) {
	if (!alliance) {
		return portals;
	}

	let rooms = await getLastData(awsCredentials, 'rooms');
	rooms = rooms[shard];
	let alliances = await getLastData(awsCredentials, 'alliances');

	let members = _.get(alliances, [alliance, 'members']);
	let allianceRooms = _.pickBy(rooms, room => _.includes(members, room.owner));

	return _.pickBy(portals, (from, to) => inReachOfRooms({rooms: allianceRooms, portal: {from, to}, distance}));
}

export async function getPortals({stageVariables, queryStringParameters}, callback) {
	let {accessKeyId, secretAccessKey} = stageVariables;
	let awsCredentials = {accessKeyId, secretAccessKey};

	queryStringParameters = queryStringParameters || {};
	const {fromPlayer, toPlayer, fromAlliance, toAlliance, distance = 10, shard = 'shard0'} = queryStringParameters;

	let portals = await getLastData(awsCredentials, 'portals');
	portals = portals[shard];

	portals = await filterForPlayer({awsCredentials, portals, player: fromPlayer, distance, shard});
	portals = await filterForPlayer({awsCredentials, portals, player: toPlayer, distance, shard});
	portals = await filterForAlliance({awsCredentials, portals, alliance: fromAlliance, distance, shard});
	portals = await filterForAlliance({awsCredentials, portals, alliance: toAlliance, distance, shard});

	portals = removeDuplicates(portals);

	callback(null, {statusCode: 200, body: JSON.stringify({portals})});
}
