import * as AWS from 'aws-sdk';

let dbConnections = {};

function getDbConnection(credentials) {
	let credentialHash = JSON.stringify(credentials);
	if (!dbConnections[credentialHash]) {
		dbConnections[credentialHash] = new AWS.DynamoDB({region: 'eu-central-1', ...credentials});
	}

	return dbConnections[credentialHash];
}

export function dbPutItem(credentials, data) {
	const dynamodb = getDbConnection(credentials);

	return dynamodb.putItem(data).promise();
}

export function dbQuery(credentials, query) {
	const dynamodb = getDbConnection(credentials);

	return dynamodb.query(query).promise();
}

export async function getLastData(credentials, type) {
	let query = {
		Limit:            1,
		TableName:        'Screeps',
		ScanIndexForward: false,
		KeyConditions:    {type: {ComparisonOperator: "EQ", AttributeValueList: [{S: type}]}}
	};

	let scanResults = await dbQuery(credentials, query);

	return JSON.parse(scanResults.Items[0].value.S);
}
