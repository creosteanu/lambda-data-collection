function roomNameToXY(name) {
	name = name.toUpperCase();

	let match = name.match(/^(\w)(\d+)(\w)(\d+)$/);
	if (!match) {
		return [undefined, undefined];
	}
	let [, hor, x, ver, y] = match;

	if (hor == 'W') {
		x = -x - 1;
	} else {
		x = +x;
	}

	if (ver == 'N') {
		y = -y - 1;
	} else {
		y = +y;
	}

	return [x, y];
}


export function getRoomLinearDistance(room1, room2) {
	let [x1, y1] = roomNameToXY(room1);
	let [x2, y2] = roomNameToXY(room2);
	let dx = Math.abs(x2 - x1);
	let dy = Math.abs(y2 - y1);

	return Math.max(dx, dy);
}
