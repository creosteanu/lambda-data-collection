export function getRoomObjects(roomName, api) {
	return new Promise(resolve => {
		api.socket.subscribe(`room:${roomName}`, event => {
			api.socket.unsubscribe(`room:${roomName}`);

			resolve(event.data.objects);
		});
	});
}
