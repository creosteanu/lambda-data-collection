import {ScreepsAPI} from 'screeps-api';

export async function getApi(credentials) {
	let api = new ScreepsAPI();

	await api.auth(credentials.email, credentials.password);
	await api.socket.connect();

	return api;
}
