import * as _ from 'lodash';
import fetch from 'node-fetch';

import {dbPutItem} from './utils';

async function updateAllianceData(awsCredentials) {

	try {
		let request = await fetch('http://www.leagueofautomatednations.com/alliances.js');
		let data = await request.json();

		let timestamp = new Date().getTime() + '';
		let Item: any = {
			type:      {S: 'alliances'},
			timestamp: {N: timestamp},
			value:     {S: JSON.stringify(data)}
		};

		await dbPutItem(awsCredentials, {Item, TableName: 'Screeps'});
	} catch (e) {
		console.log(e);
		console.log(`Failed to retrieve alliances`);
	}
}

async function updateRoomData(awsCredentials) {
	try {
		let request = await fetch('http://www.leagueofautomatednations.com/map/shard0/rooms.js');
		let shard0 = await request.json();
		shard0 = _.pickBy(shard0, room => room.level);
		request = await fetch('http://www.leagueofautomatednations.com/map/shard1/rooms.js');
		let shard1 = await request.json();
		shard1 = _.pickBy(shard1, room => room.level);

		let rooms = {shard0, shard1};

		let timestamp = new Date().getTime() + '';
		let Item: any = {
			type:      {S: 'rooms'},
			timestamp: {N: timestamp},
			value:     {S: JSON.stringify(rooms)}
		};

		await dbPutItem(awsCredentials, {Item, TableName: 'Screeps'});
	} catch (e) {
		console.log(e);
		console.log(`Failed to retrieve rooms`);
	}
}


export async function updateLoanData({awsCredentials}) {
	await updateAllianceData(awsCredentials);
	await updateRoomData(awsCredentials);
}
