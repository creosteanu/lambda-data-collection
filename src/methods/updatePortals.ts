import * as _ from 'lodash';

import {getApi, dbPutItem, getRoomObjects} from './utils';

async function pushPortal(roomName, shard, portals, api) {
	if (portals[roomName]) {
		return;
	}

	let roomObjects = await getRoomObjects(`${shard}/${roomName}`, api);
	let portal = _.find(roomObjects, object => object && object.type === 'portal');
	if (portal) {
		portals[roomName] = portal.destination.room;
		portals[portal.destination.room] = roomName;
	} else {
		portals[roomName] = false;
	}
}

async function findPortals({startPosition, shard, api}) {
	let xPosition = startPosition;
	let portals = {};

	while (xPosition > 0) {
		console.log(xPosition);
		let yPosition = xPosition;
		while (yPosition > 0) {
			await pushPortal(`W${xPosition}N${yPosition}`, shard, portals, api);
			await pushPortal(`W${xPosition}S${yPosition}`, shard, portals, api);
			await pushPortal(`E${xPosition}N${yPosition}`, shard, portals, api);
			await pushPortal(`E${xPosition}S${yPosition}`, shard, portals, api);
			await pushPortal(`W${yPosition}N${xPosition}`, shard, portals, api);
			await pushPortal(`W${yPosition}S${xPosition}`, shard, portals, api);
			await pushPortal(`E${yPosition}N${xPosition}`, shard, portals, api);
			await pushPortal(`E${yPosition}S${xPosition}`, shard, portals, api);

			yPosition -= 10;
		}

		xPosition -= 10;
	}

	portals = _.pickBy(portals, portal => portal !== false);

	return portals;
}

export async function updatePortals({screepsCredentials, awsCredentials}) {
	let api = await getApi(screepsCredentials);
	let shard0 = await findPortals({startPosition: 95, shard: 'shard0', api});
	let shard1 = await findPortals({startPosition: 45, shard: 'shard1', api});
	let portals = {shard0, shard1};
	let timestamp = new Date().getTime() + '';
	let Item = {
		type:      {S: 'portals'},
		timestamp: {N: timestamp},
		value:     {S: JSON.stringify(portals)}
	};

	await dbPutItem(awsCredentials, {Item, TableName: 'Screeps'});
}
