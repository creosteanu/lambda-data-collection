import * as _ from 'lodash';

import {getApi, dbPutItem, getLastData} from './utils';

async function updateConquests({shard, api, rankings, awsCredentials}) {
	let rooms = await getLastData(awsCredentials, 'rooms');
	rooms = rooms[shard];
	rooms = _.pickBy(rooms, (room, roomName) => !rankings[roomName]);

	let roomNames = Object.keys(rooms);

	let rawRoomData = await api.raw.game.mapStats(roomNames, 'energy1440', shard);

	for (let roomName of roomNames) {
		let taggedBy = _.get(rawRoomData, ['stats', roomName, 'sign', 'user']);
		taggedBy = taggedBy && _.get(rawRoomData, ['users', taggedBy, 'username']);
		let sign = _.get(rawRoomData, ['stats', roomName, 'sign', 'text'], '');
		let owner = rooms[roomName].owner;

		if (taggedBy && taggedBy !== owner && sign.includes('END')) {
			rankings[roomName] = {
				taggedBy,
				owner,
				taggedAtLevel: _.get(rawRoomData, ['stats', roomName, 'own', 'level'])
			};
			console.log(rankings[roomName]);
		}
	}
}

async function updateClears({shard, rankings, awsCredentials}) {
	let rooms = await getLastData(awsCredentials, 'rooms');
	rooms = rooms[shard];

	for (let roomName of Object.keys(rankings)) {
		if (!rankings[roomName].fullyCleared && !rooms[roomName]) {
			rankings[roomName].fullyCleared = true;
		}
	}
}

async function getCurrentRankings(awsCredentials) {
	let rankings;
	try {
		rankings = await getLastData(awsCredentials, 'rankings');
	} catch (e) {
		rankings = {};
	}

	return rankings;
}

export async function updateRankings({screepsCredentials, awsCredentials}) {
	let api = await getApi(screepsCredentials);
	let rankings = await getCurrentRankings(awsCredentials);
	await updateConquests({shard: 'shard0', api, awsCredentials, rankings});
	await updateClears({shard: 'shard0', awsCredentials, rankings});

	let timestamp = new Date().getTime() + '';
	let Item = {
		type:      {S: 'rankings'},
		timestamp: {N: timestamp},
		value:     {S: JSON.stringify(rankings)}
	};

	await dbPutItem(awsCredentials, {Item, TableName: 'Screeps'});
}
