import * as _ from 'lodash';

import {getLastData} from './utils';

export async function getRankings({stageVariables, queryStringParameters}, callback) {
	let {accessKeyId, secretAccessKey} = stageVariables;
	let awsCredentials = {accessKeyId, secretAccessKey};

	queryStringParameters = queryStringParameters || {};
	const {player, raw, byTags = true} = queryStringParameters;

	let rankings = await getLastData(awsCredentials, 'rankings');

	if (raw) {
		callback(null, {statusCode: 200, body: JSON.stringify(rankings)});
	} else {
		let alliances = await getLastData(awsCredentials, 'alliances');

		let rankingsByPlayer = _.transform(rankings, (acc, tag) => {
			let sourceAlliance = _.find(alliances, alliance => _.includes(alliance.members, tag.taggedBy));
			let targetAlliance = _.find(alliances, alliance => _.includes(alliance.members, tag.owner));

			if (sourceAlliance && sourceAlliance === targetAlliance) {
				return;
			}

			acc[tag.taggedBy] = acc[tag.taggedBy] || {name: tag.taggedBy, tags: 0, cleared: 0, alliance: _.get(sourceAlliance, 'name', 'independent')};

			acc[tag.taggedBy].tags++;
			acc[tag.taggedBy].cleared += tag.fullyCleared ? 1 : 0;
		}, {});

		rankingsByPlayer = _.sortBy(rankingsByPlayer, player => byTags ? -player.tags * 1000 - player.cleared : -player.cleared * 1000 - player.tags);
		_.forEach(rankingsByPlayer, (player, index) => player.rank = index + 1);

		if (player) {
			callback(null, {statusCode: 200, body: JSON.stringify(_.find(rankingsByPlayer, rank => rank.name === player))});
		} else {
			callback(null, {statusCode: 200, body: JSON.stringify(rankingsByPlayer)});
		}
	}
}
