import * as _ from 'lodash';

import * as methods from './methods';

function wrapper(toExecute) {
	return async function (event, context, callback) {
		console.log('starting execution', toExecute.name);

		context.callbackWaitsForEmptyEventLoop = false;

		try {
			await toExecute(event, callback);

			callback(null, 'Success');
		} catch (e) {
			console.log(e);
			callback(e, 'Failed');
		}
	};
}

function wrapMethods() {
	return _.transform(methods, (functions, method) => {
		functions[method.name] = wrapper(method);
	}, {});
}

export = wrapMethods();