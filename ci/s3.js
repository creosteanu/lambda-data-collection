"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const AWS = require("aws-sdk");
const fs = require("fs");
function uploadToS3() {
    return __awaiter(this, void 0, void 0, function* () {
        const project = process.argv[2];
        const branch = process.argv[3];
        const s3 = new AWS.S3({ region: 'eu-central-1' });
        yield s3.upload({ Bucket: 'lambda.ci', Key: `${project}/${branch}.zip`, Body: fs.createReadStream(`./${branch}.zip`) }).promise();
        console.log('S3 upload done');
    });
}
uploadToS3();
